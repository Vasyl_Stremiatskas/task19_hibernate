package com.stremiatskas.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;

@Entity
@Table(name = "take_order", schema = "new_restaurant")
public class TakeOrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_take_order", nullable = false)
    private Integer idTakeOrder;
    @Column(name = "order_amount", nullable = true)
    private Integer orderAmount;
    @Column(name = "ordertime", nullable = true)
    private Time ordertime;
    @Column(name = "bill", nullable = true, precision = 3)
    private BigDecimal bill;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "dish_name", referencedColumnName = "dish_name", nullable = false)
    private MenuEntity menuByDishName;
    @ManyToOne
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", nullable = false)
    private CustomerTableEntity tableById;

    public TakeOrderEntity()
    {}

    public TakeOrderEntity(String s,Integer in,Time t,BigDecimal d,String n){
        menuByDishName = new MenuEntity(s);
        orderAmount = in;
        ordertime = t;
        bill = d;
        tableById = new CustomerTableEntity(n);
    }


    public Integer getIdTakeOrder() {
        return idTakeOrder;
    }

    public void setIdTakeOrder(Integer idTakeOrder) {
        this.idTakeOrder = idTakeOrder;
    }

    public Integer getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Integer orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Time getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(Time ordertime) {
        this.ordertime = ordertime;
    }

    public BigDecimal getBill() {
        return bill;
    }

    public void setBill(BigDecimal bill) {
        this.bill = bill;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TakeOrderEntity that = (TakeOrderEntity) o;

        if (idTakeOrder != null ? !idTakeOrder.equals(that.idTakeOrder) : that.idTakeOrder != null) return false;
        if (orderAmount != null ? !orderAmount.equals(that.orderAmount) : that.orderAmount != null) return false;
        if (ordertime != null ? !ordertime.equals(that.ordertime) : that.ordertime != null) return false;
        if (bill != null ? !bill.equals(that.bill) : that.bill != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTakeOrder != null ? idTakeOrder.hashCode() : 0;
        result = 31 * result + (orderAmount != null ? orderAmount.hashCode() : 0);
        result = 31 * result + (ordertime != null ? ordertime.hashCode() : 0);
        result = 31 * result + (bill != null ? bill.hashCode() : 0);

        return result;
    }


    public MenuEntity getMenuByDishName() {
        return menuByDishName;
    }

    public void setMenuByDishName(MenuEntity menuByDishName) {
        this.menuByDishName = menuByDishName;
    }

    public CustomerTableEntity getTableById() {
        return tableById;
    }

    public void setTableById(CustomerTableEntity tableById) {
        this.tableById = tableById;
    }
}