package com.stremiatskas.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;

@Entity
@Table(name = "menu", schema = "new_restaurant")
public class MenuEntity {
    @Id
    @Column(name = "dish_name", nullable = false, length = 40)
    private String dishName;
    @Column(name = "type_in_menu", nullable = true, length = 25)
    private String typeInMenu;
    @Column(name = "dish_amount", nullable = true, length = 15)
    private String dishAmount;
    @Column(name = "price", nullable = true, precision = 2)
    private BigDecimal price;
    @OneToMany( mappedBy = "menuByDishName")
    private Collection<TakeOrderEntity> takeOrdersByDishName;

public MenuEntity(){}

public MenuEntity(String dishName){
    this.dishName = dishName;
}
    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }


    public String getTypeInMenu() {
        return typeInMenu;
    }

    public void setTypeInMenu(String typeInMenu) {
        this.typeInMenu = typeInMenu;
    }

    public String getDishAmount() {
        return dishAmount;
    }

    public void setDishAmount(String dishAmount) {
        this.dishAmount = dishAmount;
    }


    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MenuEntity that = (MenuEntity) o;

        if (dishName != null ? !dishName.equals(that.dishName) : that.dishName != null) return false;
        if (typeInMenu != null ? !typeInMenu.equals(that.typeInMenu) : that.typeInMenu != null) return false;
        if (dishAmount != null ? !dishAmount.equals(that.dishAmount) : that.dishAmount != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dishName != null ? dishName.hashCode() : 0;
        result = 31 * result + (typeInMenu != null ? typeInMenu.hashCode() : 0);
        result = 31 * result + (dishAmount != null ? dishAmount.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }


    public Collection<TakeOrderEntity> getTakeOrdersByDishName() {
        return takeOrdersByDishName;
    }

    public void setTakeOrdersByDishName(Collection<TakeOrderEntity> takeOrdersByDishName) {
        this.takeOrdersByDishName = takeOrdersByDishName;
    }
}