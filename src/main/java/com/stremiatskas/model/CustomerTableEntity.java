package com.stremiatskas.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "customer_table", schema = "new_restaurant")
public class CustomerTableEntity {
    @Id
    @Column(name = "id_table", nullable = false, length = 5)
    private String idTable;
    @OneToMany( mappedBy = "tableById")
    private Collection<TakeOrderEntity> takeOrdersByIdTable;
    @ManyToMany(mappedBy = "table")
    private Set<WaiterEntity> id_table;


    public CustomerTableEntity(){}

    public CustomerTableEntity(String s){idTable=s;}

    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerTableEntity that = (CustomerTableEntity) o;

        if (idTable != null ? !idTable.equals(that.idTable) : that.idTable != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return idTable != null ? idTable.hashCode() : 0;
    }


    public Collection<TakeOrderEntity> getTakeOrdersByIdTable() {
        return takeOrdersByIdTable;
    }

    public void setTakeOrdersByIdTable(Collection<TakeOrderEntity> takeOrdersByIdTable) {
        this.takeOrdersByIdTable = takeOrdersByIdTable;
    }

   public Set<WaiterEntity> getId_Table(){return id_table;}

    public void setId_table(Set<WaiterEntity> id_table) {
        this.id_table = id_table;
    }
}