drop database if exists New_restaurant;
create database New_restaurant
CHARACTER SET utf8 
COLLATE utf8_general_ci;
use New_restaurant;

drop table if exists Menu;
create table Menu( 
    dish_name        varchar(40) primary key, 
    type_in_menu     varchar(25),
    dish_amount      varchar(15),
    price            decimal(10,2)    
)engine = InnoDB;

drop table if exists Customer_table;
create table Customer_table(
id_table            char(5) primary key
)engine = InnoDB;

drop table if exists Take_order;
create table Take_order(
id_take_order       int primary key,
dish_name           varchar(40),
order_amount        int,
ordertime           time,
bill                decimal(10,3),
id_table            char(5) not null
)engine = InnoDB;

drop table if exists Waiter;
create table Waiter(
id_waiter   varchar(25) primary key,
name        varchar(10),
lastname    varchar(10)
)engine=InnoDB;

drop table if exists Waiter_and_Table;
create table Waiter_and_Table(
id_table    char(5)        not null,
id_waiter   varchar(25)    not null
)engine=InnoDB;


alter table Take_order
add constraint fk_order_dish_name
foreign key (dish_name)
references Menu  (dish_name)
on delete cascade on update set null;

alter table Take_order
add constraint fk_order_id_table
foreign key (id_table)
references Customer_table (dish_name)
on delete cascade on update set null;

alter table Waiter_and_Table
add constraint fk_waiter_customer_table
foreign key (id_table)
references Customer_tabel  (id_table)
on delete cascade on update set null; 

alter table Waiter_and_Table
add constraint fk_customer_table_waiter
foreign key (id_waiter)
references Waiter (id_waiter)
on delete cascade on update set null;

insert into menu values('Fillet minoun', 'meat', '250 g', 320.00);
insert into menu values('Salmon with pesto', 'fish', '280 g', 320.00);
insert into menu values('Salad with seafood', 'salat', '300 g', 200.00);
insert into menu values('Rabbit paste', 'appetizers', '180 g', 100.00);
insert into menu values('Fondan', 'dessert', '120 g', 80.00);
insert into menu values('Dorado grill', 'fish', '250 g', 280.00);
insert into menu values('Salad Cessare', 'salad', '270 g', 110.00);
insert into menu values('Carot cake', 'dessert', '250 g', 90.00);

insert into customer_table values('1a');
insert into customer_table values('1b');
insert into customer_table values('2a');
insert into customer_table values('3v');
insert into customer_table values('4a');
insert into customer_table values('4b');
insert into customer_table values('4v');


insert into take_order values(1,'Salmon with pesto',2,'190700',640,'2a');
insert into take_order values(2,'Fondan',1,'190800',80,'1a');
insert into take_order values(3,'Carrot cake', 3,'191500',270,'2a');
insert into take_order values(4,'Fillet minoun',1,'193000',280,'4b');
insert into take_order values(5,'Dorado grill',2,'193000',500,'4b');
insert into take_order values(6,'Salad with seafood',4,'193700',800,'4v');
insert into take_order values(7,'Fondan',2,'195300',160,'4v');

insert into waiter values('Zori','Zoryana','Dukunska');
insert into waiter values('Dakota','Natalia','Trach');
insert into waiter values('Andrew','Andrii','Salo');
insert into waiter values('Andi','Andrii','Moroznuskuy');
insert into waiter values('Leo','Lev','Ginskiy');

insert into waiter_and_table values ('1a','Andi');
insert into waiter_and_table values ('1b','Zori');
insert into waiter_and_table values ('2a','Dakota');
insert into waiter_and_table values ('3v','Leo');
insert into waiter_and_table values ('4a','Andrew');
insert into waiter_and_table values ('4b','Dakota');
insert into waiter_and_table values ('4v','Andi');

delimiter //
use new_restaurant //

CREATE PROCEDURE TakeOrder(id_order_table int, amount decimal(10,3), dish_add varchar(25))
begin

update Custumer_table
 set bill = bill + amount*(select price from Dish where  dish_name = dish_add) 
                                              where id_order_table = id_table;
                                              
insert into take_order (id_take_order, dish_name, order_amount, ordertime, bill,id_order_table  ) values ( id_take_order, dish_add, order_amount, ordertime, bill,id_order_table);

end;
delimiter //

